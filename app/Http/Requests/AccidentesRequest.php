<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccidentesRequest extends FormRequest
{
    public function messages()
    {
        return [
            'name.unique'          => 'El título ingresado no puede ser usado ya esta registrado!',
        ];
    }

    public function rules()
    {
        return [
            'name'        => 'required|max:50|unique:accidentes,name,' . $this->id,
        ];
    }
}
