<?php

use Illuminate\Http\Request;

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Afps;

class AfpsController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $afps = Afps::orderBy('id', 'desc')->paginate(10);
        }
        else{
            $afps = Afps::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $afps->total(),
                'current_page' => $afps->currentPage(),
                'per_page'     => $afps->perPage(),
                'last_page'    => $afps->lastPage(),
                'from'         => $afps->firstItem(),
                'to'           => $afps->lastItem(),
            ],
            'afps' => $afps
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $afps = new Afps();
        $afps->name = strtoupper($request->name);
        $afps->description = $request->description;
        $afps->creathor = 1;
        $afps->status = '1';
        $afps->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $afps = Afps::findOrFail($request->id);
        $afps->name = strtoupper($request->name);
        $afps->description = $request->description;
        $afps->status = '1';
        $afps->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $afps = Afps::findOrFail($request->id);
        $afps->status = '0';
        $afps->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $afps = Afps::findOrFail($request->id);
        $afps->status = '1';
        $afps->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $afps = Afps::findOrFail($request->id);
        $afps->delete();
    }

    public function selectGrupo(Request $request){
        $afps = Afps::where('status','=','1')
        ->select('id','name')->orderBy('name', 'asc')->get();
        return ['afps' => $afps];
    }
}

