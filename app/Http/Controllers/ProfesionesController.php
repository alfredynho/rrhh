<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Profesion;
use PDF;

class ProfesionesController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $profesiones = Profesion::orderBy('id', 'desc')->paginate(10);
        }
        else{
            $profesiones = Profesion::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $profesiones->total(),
                'current_page' => $profesiones->currentPage(),
                'per_page'     => $profesiones->perPage(),
                'last_page'    => $profesiones->lastPage(),
                'from'         => $profesiones->firstItem(),
                'to'           => $profesiones->lastItem(),
            ],
            'profesiones' => $profesiones
        ];
    }


    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $profesion = new Profesion();
        $profesion->nombre = $request->nombre;
        $profesion->estado = '1';
        $profesion->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $profesion = Profesion::findOrFail($request->id);
        $profesion->nombre = $request->nombre;
        $profesion->estado = '1';
        $profesion->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $profesion = Profesion::findOrFail($request->id);
        $profesion->estado = '0';
        $profesion->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $profesion = Profesion::findOrFail($request->id);
        $profesion->estado = '1';
        $profesion->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $profesion = Profesion::findOrFail($request->id);
        $profesion->delete();
    }

    public function selectProfesion(Request $request){
        if (!$request->ajax()) return redirect('/');
        $filtro = $request->filtro;
        $profesiones = Profesion::where('nombre', 'like', '%'. $filtro . '%')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['profesiones' => $profesiones];
    }

}


