<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\TiposAccidentes;
use DB;

class TiposAccidentesController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $taccidentes = DB::table('tiposaccidentes')
            ->join('accidentes','tiposaccidentes.id_unidad','=','accidentes.id')
            ->select('tiposaccidentes.*','accidentes.name as nombre_accidente')
            ->orderBy('tiposaccidentes.id','desc')->paginate(10);
        }
        else{
            $taccidentes = DB::table('tiposaccidentes')
            ->join('accidentes','tiposaccidentes.id_unidad','=','accidentes.id')
            ->select('accidentes.*','tiposaccidentes.name as nombre_accidente')
            ->where('accidentes.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('accidentes.id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $taccidentes->total(),
                'current_page' => $taccidentes->currentPage(),
                'per_page'     => $taccidentes->perPage(),
                'last_page'    => $taccidentes->lastPage(),
                'from'         => $taccidentes->firstItem(),
                'to'           => $taccidentes->lastItem(),
            ],
            'taccidentes' => $taccidentes
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
            $taccidentes = new TiposAccidentes();
            $taccidentes->id_status = $request->id_status;
            $taccidentes->name = $request->name;
            $taccidentes->id_client = $request->id_client;
            $taccidentes->note = $request->note;
            $taccidentes->code = $request->code;
            $taccidentes->id_unidad = $request->id_unidad;
            $taccidentes->is_published = '1';
            $taccidentes->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $taccidentes = TiposAccidentes::findOrFail($request->id);
        $taccidentes->id_status = $request->id_status;
        $taccidentes->name = $request->name;
        $taccidentes->id_client = $request->id_client;
        $taccidentes->note = $request->note;
        $taccidentes->code = $request->code;
        $taccidentes->id_unidad = $request->id_unidad;
        $taccidentes->is_published = '1';
        $taccidentes->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $taccidentes = TiposAccidentes::findOrFail($request->id);
        $taccidentes->is_published = '0';
        $taccidentes->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $taccidentes = TiposAccidentes::findOrFail($request->id);
        $taccidentes->is_published = '1';
        $taccidentes->save();
    }
}
