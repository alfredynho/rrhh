<?php

namespace App\Http\Controllers;
use App\Models\Unidad;
use App\Models\Profesion;
use App\Models\Cargo;
use App\Models\Personal;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        // if (!$request->ajax()) return redirect('/');

        $unidades = Unidad::get()->count();
        $profesiones = Profesion::get()->count();
        $cargos = Cargo::get()->count();
        $personal = Personal::get()->count();

        return [
            'unidades' => $unidades,
            'profesion' => $profesiones,
            'cargo' => $cargos,
            'personal' => $personal
        ];
    }
}
