<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Ocupaciones;
use DB;

class OcupacionesController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $ocupaciones = DB::table('ocupaciones')
            ->join('groups','ocupaciones.id_grupo','=','groups.id')
            ->select('ocupaciones.*','groups.name as nombre_grupo')
            ->orderBy('ocupaciones.id','desc')->paginate(10);
        }
        else{
            $ocupaciones = DB::table('ocupaciones')
            ->join('groups','ocupaciones.id_grupo','=','groups.id')
            ->select('ocupaciones.*','groups.name as nombre_grupo')
            ->orderBy('ocupaciones.id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $ocupaciones->total(),
                'current_page' => $ocupaciones->currentPage(),
                'per_page'     => $ocupaciones->perPage(),
                'last_page'    => $ocupaciones->lastPage(),
                'from'         => $ocupaciones->firstItem(),
                'to'           => $ocupaciones->lastItem(),
            ],
            'ocupaciones' => $ocupaciones
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
            $ocupaciones = new Ocupaciones();
            $ocupaciones->name = $request->name;
            $ocupaciones->code = $request->code;
            $ocupaciones->creathor = 1;
            $ocupaciones->group = $request->name;
            $ocupaciones->description = $request->description;
            $ocupaciones->id_grupo = $request->id_grupo;
            $ocupaciones->status = '1';
            $ocupaciones->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $ocupaciones = Ocupaciones::findOrFail($request->id);
        $ocupaciones->name = $request->name;
        $ocupaciones->code = $request->code;
        $ocupaciones->creathor = 1;
        $ocupaciones->group = $request->name;
        $ocupaciones->description = $request->description;
        $ocupaciones->id_grupo = $request->id_grupo;
        $ocupaciones->status = '1';
        $ocupaciones->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $ocupaciones = Ocupaciones::findOrFail($request->id);
        $ocupaciones->status = '0';
        $ocupaciones->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $ocupaciones = Ocupaciones::findOrFail($request->id);
        $ocupaciones->status = '1';
        $ocupaciones->save();
    }
}
