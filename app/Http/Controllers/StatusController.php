<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Status;
use App\Http\Controllers\Controller;

class StatusController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $status = Status::orderBy('id', 'desc')->paginate(10);
        }
        else{
            $status = Status::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $status->total(),
                'current_page' => $status->currentPage(),
                'per_page'     => $status->perPage(),
                'last_page'    => $status->lastPage(),
                'from'         => $status->firstItem(),
                'to'           => $status->lastItem(),
            ],
            'status' => $status
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $status = new Status();
        $status->name = $request->name;
        $status->description = $request->description;
        $status->status = '1';
        $status->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $status = Status::findOrFail($request->id);
        $status->name = $request->name;
        $status->description = $request->description;
        $status->status = '1';
        $status->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $status = Status::findOrFail($request->id);
        $status->status = '0';
        $status->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $status = Status::findOrFail($request->id);
        $status->status = '1';
        $status->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $status = Status::findOrFail($request->id);
        $status->delete();
    }

    public function selectStatus(Request $request){
        $status = Status::where('status','=','1')
        ->select('id','name')->orderBy('name', 'asc')->get();
        return ['status' => $status];
    }
}
