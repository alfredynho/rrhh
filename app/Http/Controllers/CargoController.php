<?php

namespace App\Http\Controllers;
use App\Models\Cargo;
use Illuminate\Http\Request;
use DB;

class CargoController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $cargos = DB::table('cargo')
            ->join('unidad','cargo.id_unidad','=','unidad.id')
            ->select('cargo.*','unidad.nombre as nombre_unidad')
            ->orderBy('cargo.id','desc')->paginate(10);

        }
        else{
            $cargos = DB::table('cargo')
            ->join('unidad','cargo.id_unidad','=','unidad.id')
            ->select('cargo.*','unidad.nombre as nombre_unidad')
            ->where('cargo.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('cargo.id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $cargos->total(),
                'current_page' => $cargos->currentPage(),
                'per_page'     => $cargos->perPage(),
                'last_page'    => $cargos->lastPage(),
                'from'         => $cargos->firstItem(),
                'to'           => $cargos->lastItem(),
            ],
            'cargos' => $cargos
        ];
    }

    public function cargoPdf(){
        $cargos = Cargo::all();

        $cont = Cargo::count();

        $pdf = PDF::loadView('pdf.cargos',['cargos'=>$cargos,'cont'=>$cont])->setPaper('a4', 'portrait');
        return $pdf->stream('cargos.pdf');
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = new Cargo();
        $cargo->nombre = strtoupper($request->nombre);
        $cargo->id_unidad = $request->id_unidad;
        $cargo->estado = '1';
        $cargo->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);

        $cargo->nombre = strtoupper($request->nombre);
        $cargo->id_unidad = $request->id_unidad;
        $cargo->estado = '1';
        $cargo->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);
        $cargo->estado = '0';
        $cargo->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);
        $cargo->estado = '1';
        $cargo->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);
        $cargo->delete();
    }

    public function selectCargo(Request $request){
        if (!$request->ajax()) return redirect('/');
        $cargos = Cargo::where('estado','=','1')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['cargos' => $cargos];
    }

}
