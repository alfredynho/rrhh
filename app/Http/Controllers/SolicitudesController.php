<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Models\Solicitudes;

class SolicitudesController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $solicitudes = Solicitudes::orderBy('id', 'desc')->paginate(10);
            // $cargos = DB::table('cargo')
            // ->join('unidad','cargo.id_unidad','=','unidad.id')
            // ->select('cargo.*','unidad.nombre as nombre_unidad')
            // ->orderBy('cargo.id','desc')->paginate(10);
        }
        else{
            $solicitudes = Solicitudes::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
            // $cargos = DB::table('cargo')
            // ->join('unidad','cargo.id_unidad','=','unidad.id')
            // ->select('cargo.*','unidad.nombre as nombre_unidad')
            // ->where('cargo.'.$criterio, 'like', '%'. $buscar . '%')
            // ->orderBy('cargo.id','desc')->paginate(10);
        }

        $_horas_personal = Solicitudes::where('id',"=",1);

        return [
            'pagination' => [
                'total'        => $solicitudes->total(),
                'current_page' => $solicitudes->currentPage(),
                'per_page'     => $solicitudes->perPage(),
                'last_page'    => $solicitudes->lastPage(),
                'from'         => $solicitudes->firstItem(),
                'to'           => $solicitudes->lastItem(),
            ],
            'solicitudes' => $solicitudes,
            'h_personal' => $_horas_personal
        ];
    }

    public function cargoPdf(){
        $cargos = Cargo::all();

        $cont = Cargo::count();

        $pdf = PDF::loadView('pdf.cargos',['cargos'=>$cargos,'cont'=>$cont])->setPaper('a4', 'portrait');
        return $pdf->stream('cargos.pdf');
    }

    public function store(Request $request)
    {
        // if (!$request->ajax()) return redirect('/');
        $cargo = new Solicitudes();
        $cargo->razon = $request->id_personal;
        $cargo->personal_id = $request->id_personal;
        $cargo->hora = $request->horas;
        $cargo->descripcion = strtoupper($request->descripcion);
        $cargo->save();
    }

    public function storeVacaciones(Request $request)
    {
        // if (!$request->ajax()) return redirect('/');
        $cargo = new Solicitudes();
        $cargo->razon = $request->id_personal;
        $cargo->personal_id = $request->id_personal;
        $cargo->fecha_inicio = $request->fecha_inicio;
        $cargo->fecha_finalizacion = $request->fecha_finalizacion;
        $cargo->descripcion = strtoupper($request->descripcion);
        $cargo->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);

        $cargo->nombre = strtoupper($request->nombre);
        $cargo->id_unidad = $request->id_unidad;
        $cargo->estado = '1';
        $cargo->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);
        $cargo->estado = '0';
        $cargo->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);
        $cargo->estado = '1';
        $cargo->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $cargo = Cargo::findOrFail($request->id);
        $cargo->delete();
    }

    public function selectCargo(Request $request){
        if (!$request->ajax()) return redirect('/');
        $cargos = Cargo::where('estado','=','1')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['cargos' => $cargos];
    }

}

