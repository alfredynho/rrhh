<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Unidad;
use PDF;

class UnidadController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $unidades = Unidad::orderBy('id', 'desc')->paginate(10);
        }
        else{
            $unidades = Unidad::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $unidades->total(),
                'current_page' => $unidades->currentPage(),
                'per_page'     => $unidades->perPage(),
                'last_page'    => $unidades->lastPage(),
                'from'         => $unidades->firstItem(),
                'to'           => $unidades->lastItem(),
            ],
            'unidades' => $unidades
        ];
    }

    public function unidadesPdf(){
        $unidades = Unidad::all();

        $cont = Unidad::count();

        $pdf = PDF::loadView('pdf.unidades',['unidades'=>$unidades,'cont'=>$cont])->setPaper('a4', 'portrait');
        return $pdf->stream('unidades.pdf');
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $unidad = new Unidad();
        $unidad->codigo = $request->codigo;
        $unidad->nombre = $request->nombre;
        $unidad->estado = '1';
        $unidad->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $unidad = Unidad::findOrFail($request->id);
        $unidad->nombre = $request->nombre;
        $unidad->codigo = $request->codigo;
        $unidad->estado = '1';
        $unidad->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $unidad = Unidad::findOrFail($request->id);
        $unidad->estado = '0';
        $unidad->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $unidad = Unidad::findOrFail($request->id);
        $unidad->estado = '1';
        $unidad->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $unidad = Unidad::findOrFail($request->id);
        $unidad->delete();
    }

    public function selectUnidad(Request $request){
        if (!$request->ajax()) return redirect('/');
        $unidades = Unidad::where('estado','=','1')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['unidades' => $unidades];
    }

}
