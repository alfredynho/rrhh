<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Accidentes;
use App\Http\Requests\AccidentesRequest;

class AccidentesController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $accidentes = Accidentes::orderBy('id', 'asc')->paginate(10);
        }
        else{
            $accidentes = Accidentes::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'asc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $accidentes->total(),
                'current_page' => $accidentes->currentPage(),
                'per_page'     => $accidentes->perPage(),
                'last_page'    => $accidentes->lastPage(),
                'from'         => $accidentes->firstItem(),
                'to'           => $accidentes->lastItem(),
            ],
            'accidentes' => $accidentes
        ];
    }

    public function store(AccidentesRequest $request)
    {
        if (!$request->ajax()) return redirect('/');
        $accidentes = new Accidentes();
        $accidentes->name = $request->name;
        $accidentes->creathor = 1;
        $accidentes->description = $request->description;
        $accidentes->status = '1';
        $accidentes->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $accidentes = Accidentes::findOrFail($request->id);
        $accidentes->name = $request->name;
        $accidentes->creathor = 1;
        $accidentes->description = $request->description;
        $accidentes->status = '1';
        $accidentes->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
            $accidentes = Accidentes::findOrFail($request->id);
            $accidentes->status = '0';
            $accidentes->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
            $accidentes = Accidentes::findOrFail($request->id);
            $accidentes->status = '1';
            $accidentes->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
            $accidentes = Accidentes::findOrFail($request->id);
            $accidentes->delete();
    }

    public function selectUnits(Request $request){
        $accidentes = Accidentes::where('status','=','1')
        ->select('id','name')
        ->orderBy('name', 'asc')->get();
        return ['acci$accidentes' => $accidentes];
    }
}
