<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Provider;
use \App\Person;

class ProviderController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $products = DB::table('products')
            ->join('categories','products.id_categoria','=','categories.id')
            ->select('products.*','categories.name as category_name')
            ->orderBy('products.id','desc')->paginate(10);
        }
        else{
            $products = DB::table('products')
            ->join('categories','products.id_categoria','=','categories.id')
            ->select('products.*','categories.name as category_name')
            ->where('products.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('products.id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $products->total(),
                'current_page' => $products->currentPage(),
                'per_page'     => $products->perPage(),
                'last_page'    => $products->lastPage(),
                'from'         => $products->firstItem(),
                'to'           => $products->lastItem(),
            ],
            'products' => $products
        ];
    }


    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $product = new Product();
        $product->id_categoria = $request->id_categoria;
        $product->name = $request->name;
        $product->gerente = $request->gerente;
        $product->description = $request->description;
        $product->productos = $request->productos;
        $product->address = $request->address;
        $product->socials = $request->socials;
        $product->status = '1';
        $product->image = 'imagen';
        $product->save();
    }

    public function update(Request $request)
    {
        $product = Product::findOrFail($request->id);
        $product->id_categoria = $request->id_categoria;
        $product->name = $request->name;
        $product->gerente = $request->gerente;
        $product->address = $request->address;
        $product->socials = $request->socials;
        $product->description = $request->description;
        $product->status = '1';
        $product->image = "imagen";
        $product->save();
    }


    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $product = Product::findOrFail($request->id);
        $product->status = '0';
        $product->save();
    }


    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $product = Product::findOrFail($request->id);
        $product->status = '1';
        $product->save();
    }
}
