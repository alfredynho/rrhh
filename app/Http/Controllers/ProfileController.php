<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Personal;

class ProfileController extends Controller
{
    public function personal_detalle(Request $request){
        $personal = Personal::findOrFail($request->id);

        return [
            'personal' => $personal
        ];
    }
}
