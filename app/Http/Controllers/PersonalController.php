<?php

namespace App\Http\Controllers;

use App\Models\Personal;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use PDF;
use Illuminate\Support\Carbon;
use DateTime;

class PersonalController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $personal = Personal::orderBy('id', 'desc')->paginate(10);
        }
        else{
            $personal = Personal::where($criterio, 'like', '%'. $buscar . '%')->orderBy('id', 'desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $personal->total(),
                'current_page' => $personal->currentPage(),
                'per_page'     => $personal->perPage(),
                'last_page'    => $personal->lastPage(),
                'from'         => $personal->firstItem(),
                'to'           => $personal->lastItem(),
            ],
            'personal' => $personal
        ];
    }

    public function cargoPdf(){
        $personal = Personal::all();

        $cont = Personal::count();

        $pdf = PDF::loadView('pdf.personal.detalle_personal',['personal'=>$personal,'cont'=>$cont])->setPaper('a4', 'portrait');
        return $pdf->stream('personal.pdf');

    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $personal = new Personal();

        $personal->cedula = strtoupper($request->get('cedula'));
        $personal->nombres = strtoupper($request->get('nombres'));
        $personal->paterno = strtoupper($request->get('paterno'));
        $personal->materno = strtoupper($request->get('materno'));
        $personal->caja = strtoupper($request->get('caja'));
        $personal->id_cargo = $request->get('id_cargo');
        $personal->id_profesion = $request->get('id_profesion');
        $personal->correo = $request->get('correo');
        $personal->fecha_ingreso = $request->get('f_ingreso');
        $personal->fecha_nacimiento = $request->get('f_nacimiento');
        $personal->tipo_personal = $request->get('tipo_personal');
        $personal->nacionalidad = $request->get('nacionalidad');

        $_slug = mb_strtolower((str_replace(" ","-",$request->nombres.''.$request->paterno.''.$request->materno)),'UTF-8');
        $personal->slug = $_slug;

        $personal->estado_civil = $request->get('estado_civil');
        $personal->celular = $request->get('celular');
        $personal->genero = $request->get('genero');
        $personal->direccion = $request->get('direccion');
        $personal->afp = $request->get('afp');
        $personal->seguro = 1;
        $personal->estado = 1;

        $fecha_nacimiento = explode("-",$request->get('f_nacimiento'));
        // 2 es dia
        // 1 es mes
        // 0 es año

        // $edad = Carbon::createFromDate(1991,7,8)->age; // 43

        // $formateo_fecha = $fecha_nacimiento[0].",".$fecha_nacimiento[2].",".$fecha_nacimiento[1];

        // $fecha = explode("-", $anio);

        // $FechaEjecucion = $fecha[0]."-".$fecha[2]."-".$fecha[1];
        $anio =  $fecha_nacimiento[0];
        $mes = ltrim($fecha_nacimiento[1],0);
        $dia = ltrim($fecha_nacimiento[2],0);

        $personal->edad = Carbon::createFromDate(intval($anio),intval($dia),intval($mes))->age;

        $personal->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $personal = Personal::findOrFail($request->id);
        $personal->nombre = $request->nombre;
        $personal->codigo = $request->codigo;
        $personal->estado = '1';
        $personal->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $personal = Personal::findOrFail($request->id);
        $personal->estado = '0';
        $personal->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $personal = Personal::findOrFail($request->id);
        $personal->estado = '1';
        $personal->save();
    }

    public function eliminar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $personal = Personal::findOrFail($request->id);
        $personal->delete();
    }

    public function selectUnidad(Request $request){
        if (!$request->ajax()) return redirect('/');
        $personal = Personal::where('estado','=','1')
        ->select('id','nombre')
        ->orderBy('nombre', 'asc')->get();

        return ['personal' => $personal];
    }

    public function personalPdf(Request $request){
        $personal = Personal::findOrFail($request->id);
        $cont = Personal::count();

        $pdf = PDF::loadView('pdf.personal',['personal'=>$personal,'cont'=>$cont])->setPaper('a4', 'portrait');
        return $pdf->stream('personal.pdf');
    }

    public function personalListadoPdf(Request $request){
        $personal = Personal::all();
        $cont = Personal::count();

        $jaime = Personal::findOrFail(22);

        $carbon = new \Carbon\Carbon();
        $date = $carbon->format('y-m-d');
        $ahora = $carbon->now();

        // $edad = Carbon::createFromDate(1991,7,8)->age; // 43
        // $dif = $jaime->fecha_ingreso->diff($date);
        // $dif = $jaime->fecha_ingreso->month;

        // 2 es dia
        // 1 es mes
        // 0 es año


        $anio = $jaime->fecha_nacimiento;

        $fecha = explode("-", $anio);

        // $FechaEjecucion = $fecha[0]."-".$fecha[2]."-".$fecha[1];

        $anio =  ltrim($fecha[0]);
        $mes = ltrim($fecha[1],0);
        $dia = ltrim($fecha[2],0);

        // $edad = Carbon::createFromDate(intval($anio[0]),intval($anio[2]),intval($anio[1]))->age; // 43

        // dump(gettype(intval($anio[0])));

        // $dd = Carbon::createFromDate($anio[0],substr($anio[2],1),substr($anio[1],1))->age;

        // dump($dif);

        // if ($ahora->isWeekend()){
        //     // dump($dif);
        //     dump("fin de semana");
        // }


        $anio_ingreso = $jaime->fecha_ingreso;
        dump($anio_ingreso);

        $fecha1= new DateTime("2020-05-14");
        $fecha2= new DateTime($jaime->fecha_ingreso);
        $diff = $fecha1->diff($fecha2);

        if ($diff->days >= 365){
            dump("Puedo tener vacaciones");
        }else{
            dump("No puede sacar vacaciones");
        }

        $pdf = PDF::loadView('pdf.listadoPersonal',['personal'=>$personal,'cont'=>$cont])->setPaper('a4', 'landscape');
        return $pdf->stream('personal_listado.pdf');
    }

    public function selectPersonal(Request $request){
        if (!$request->ajax()) return redirect('/');
        $personal = Personal::where('estado','=','1')
        ->select('id','nombres','materno','paterno')
        ->orderBy('nombres', 'asc')->get();

        return ['personal' => $personal];
    }
}

