<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Tasks;
use DB;

class TasksController extends Controller
{
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar==''){
            $tasks = DB::table('tasks')
            ->join('units','tasks.id_unidad','=','units.id')
            ->join('clients','tasks.id_client','=','clients.id')
            ->join('status','tasks.id_status','=','status.id')
            ->select('tasks.*','units.name as nombre_unidad','clients.business_name as nombre_cliente', 'status.name as nombre_estado')
            ->orderBy('tasks.id','desc')->paginate(10);


            // ->join('categories','products.id_categoria','=','categories.id')
            // ->select('products.*','categories.name as category_name')
            // ->orderBy('products.id','desc')->paginate(5);
        }
        else{
            $tasks = DB::table('tasks')
            ->join('units','tasks.id_unidad','=','units.id')
            ->select('tasks.*','units.name as nombre_unidad')
            ->where('tasks.'.$criterio, 'like', '%'. $buscar . '%')
            ->orderBy('tasks.id','desc')->paginate(10);
        }

        return [
            'pagination' => [
                'total'        => $tasks->total(),
                'current_page' => $tasks->currentPage(),
                'per_page'     => $tasks->perPage(),
                'last_page'    => $tasks->lastPage(),
                'from'         => $tasks->firstItem(),
                'to'           => $tasks->lastItem(),
            ],
            'tasks' => $tasks
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
            $tasks = new Tasks();
            $tasks->id_status = $request->id_status;
            $tasks->name = $request->name;
            $tasks->id_client = $request->id_client;
            $tasks->note = $request->note;
            $tasks->code = $request->code;
            $tasks->id_unidad = $request->id_unidad;
            $tasks->is_published = '1';
            $tasks->save();
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $tasks = Tasks::findOrFail($request->id);
        $tasks->id_status = $request->id_status;
        $tasks->name = $request->name;
        $tasks->id_client = $request->id_client;
        $tasks->note = $request->note;
        $tasks->code = $request->code;
        $tasks->id_unidad = $request->id_unidad;
        $tasks->is_published = '1';
        $tasks->save();
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $tasks = Tasks::findOrFail($request->id);
        $tasks->is_published = '0';
        $tasks->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $tasks = Tasks::findOrFail($request->id);
        $tasks->is_published = '1';
        $tasks->save();
    }
}
