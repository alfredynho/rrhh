<?php

namespace App\Http\Controllers;

use App\User;
use App\Person;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        $buscar = $request->buscar;
        $criterio = $request->criterio;

        if ($buscar == '') {
            $personas = User::join('persons', 'users.id', '=', 'persons.id')
                ->join('roles', 'users.id_rol', '=', 'roles.id')
                ->select(
                    'persons.id',
                    'persons.name',
                    'persons.nit',
                    'persons.address',
                    'persons.telephone',
                    'persons.email',
                    'users.usuario',
                    'users.password',
                    'users.status',
                    'users.id_rol',
                    'roles.name as rol'
                )
                ->orderBy('persons.id', 'desc')->paginate(3);
        } else {
            $personas = User::join('persons', 'users.id', '=', 'persons.id')
                ->join('roles', 'users.id_rol', '=', 'roles.id')
                ->select(
                    'persons.id',
                    'persons.name',
                    'persons.nit',
                    'persons.address',
                    'persons.telephone',
                    'persons.email',
                    'users.usuario',
                    'users.password',
                    'users.status',
                    'users.id_rol',
                    'roles.name as rol'
                )
                ->where('persons.' . $criterio, 'like', '%' . $buscar . '%')
                ->orderBy('persons.id', 'desc')->paginate(3);
        }


        return [
            'pagination' => [
                'total'        => $personas->total(),
                'current_page' => $personas->currentPage(),
                'per_page'     => $personas->perPage(),
                'last_page'    => $personas->lastPage(),
                'from'         => $personas->firstItem(),
                'to'           => $personas->lastItem(),
            ],
            'personas' => $personas
        ];
    }

    public function store(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try {
            DB::beginTransaction();
            $persona = new Person();
            $persona->name = $request->nombre;
            $persona->nit = $request->nit;
            $persona->address = $request->direccion;
            $persona->telephone = $request->telephone;
            $persona->email = $request->email;
            $persona->save();

            $user = new User();
            $user->usuario = $request->usuario;
            $user->password = bcrypt($request->password);
            $user->status = '1';
            $user->id_rol = $request->idrol;

            $user->id = $persona->id;

            $user->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    public function update(Request $request)
    {
        if (!$request->ajax()) return redirect('/');

        try {
            DB::beginTransaction();

            //Buscar primero el proveedor a modificar
            $user = User::findOrFail($request->id);

            $persona = Person::findOrFail($user->id);

            $persona->name = $request->name;
            $persona->nit = $request->nit;
            $persona->address = $request->address;
            $persona->telephone = $request->telephone;
            $persona->email = $request->email;
            $persona->save();


            $user->usuario = $request->usuario;
            $user->password = bcrypt($request->password);
            $user->status = '1';
            $user->id_rol = $request->idrol;
            $user->save();


            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }
    }

    public function desactivar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->status = '0';
        $user->save();
    }

    public function activar(Request $request)
    {
        if (!$request->ajax()) return redirect('/');
        $user = User::findOrFail($request->id);
        $user->status = '1';
        $user->save();
    }

}
