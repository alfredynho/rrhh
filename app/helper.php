<?php

 function getUser(){
    $id = auth()->user()->id;
    $permiso = DB::table('personal as p')->where('p.id',$id)->first();

    return $permiso;
    // $permiso= DB::table('model_has_permissions as m')->join('permissions as p','m.permission_id','=','p.id')->where('m.model_id',$id)->where('p.name','crm_ver_todo')->first();
    // if($permiso!=null) return '1'; else return '0';
 }


 function userId(){
    return auth()->user()->id;
 }

 function estadoCivil($estado){
     $_estado = "";

    switch ($estado) {
        case 1:
            $_estado = "Soltero/a";
        case 2:
            $_estado = "Casado/a";
        case 3:
            $_estado = "Separado/a";
        case 4:
            $_estado = "Divorciado/a";
        case 5:
            $_estado = "Viudo/a";
    }

     return $_estado;
 }

