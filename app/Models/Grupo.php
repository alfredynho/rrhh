<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table = 'groups';

    protected $fillable = [
        'name',
        'creathor',
        'description',
        'status'
    ];
}
