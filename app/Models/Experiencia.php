<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Experiencia extends Model
{
    protected $table = 'experiencia';

    protected $fillable = [
        'entidad',
        'pais',
        'cargo',
        'desde',
        'hasta',
    ];
}
