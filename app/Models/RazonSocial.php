<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RazonSocial extends Model
{
    protected $table = 'razonsocials';

    protected $fillable = [
        'name',
        'creathor',
        'description',
        'status'
    ];
}
