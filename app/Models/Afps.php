<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Afps extends Model
{
    protected $table = 'afps';

    protected $fillable = [
        'name',
        'creathor',
        'description',
        'status'
    ];
}

