<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Forms extends Model
{
    protected $table = 'forms';

    protected $fillable = [
        'apcasada',
        'paterno',
        'materno',
        'nombres',
        'fecha_nacimiento',
        'gender',
        'age',
        'afp',
        'razon_social',
        'numero_patronal',
        'id_ocupacion',
        'fecha_inicio_laboral',
        'entrenamiento',
        'instruccion',
        'fecha_recepcion',
        'diagnostico',
        'idaccidente',
        'idtipoaccidente',
        'observaciones',
        'status'
    ];
}

