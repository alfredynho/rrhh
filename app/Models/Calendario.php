<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendario extends Model
{
    protected $table = 'calendario';

    protected $fillable = [
        'id',
        'nombre_actividad',
        'inicio',
        'fin',
        'description',
    ];
}
