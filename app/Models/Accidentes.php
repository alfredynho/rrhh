<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accidentes extends Model
{
    protected $table = 'accidentes';

    protected $fillable = [
        'name',
        'creathor',
        'description',
        'status'
    ];
}
