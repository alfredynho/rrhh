<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitudes extends Model
{
    protected $table = 'solicitudes';

    protected $fillable = [
        'id',
        'razon',
        'personal_id',
        'cantidad_dias',
        'fecha_inicio',
        'fecha_finalizacion',
        'descripcion',
        'saldo_horas',
        'saldo_vacaciones',
        'hora',
    ];
}
