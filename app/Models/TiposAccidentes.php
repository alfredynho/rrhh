<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TiposAccidentes extends Model
{
    protected $table = 'tiposaccidentes';

    protected $fillable = [
        'code',
        'id_accidente',
        'creathor',
        'description',
        'status'
    ];
}

