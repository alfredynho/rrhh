<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parentesco extends Model
{
    protected $table = 'parentesco';

    protected $fillable = [
        'cedula',
        'nombres',
        'paterno',
        'materno',
        'parentesco',
        'fecha_nacimiento',
        'profesion',
    ];
}
