<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personal extends Model
{
    protected $table = 'personal';

    protected $fillable = [
        'id',
        'cedula',
        'nombres',
        'paterno',
        'materno',
        'celular',
        'nacionalidad',
        'tipo_personal',
        'fecha_nacimiento',
        'caja',
        'slug',
        'id_cargo',
        'id_profesion',
        'correo',
        'fecha_ingreso',
        'estado_civil',
        'celular',
        'genero',
        'direccion',
        'afp',
        'personal',
        'seguro',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
