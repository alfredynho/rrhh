<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ocupaciones extends Model
{
    protected $table = 'ocupaciones';

    protected $fillable = [
        'name',
        'code',
        'group',
        'creathor',
        'description',
        'status'
    ];
}
