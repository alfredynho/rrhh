<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capacitacion extends Model
{
    protected $table = 'capacitacion';

    protected $fillable = [
        'nivel',
        'institucion',
        'desde',
        'hasta',
        'situacion',
        'id_capacitacion',
    ];
}
