<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## DevLaravel Backend

**OpenStack** Backend Project DevLaravel
### Tecnologías

  * [Laravel](https://laravel.com/)
  * [Webmin](https://www.djangoproject.com/)
  * [Postgres](https://www.postgresql.org/)
  * [Vuejs-Fork](https://vuejs.org/)
  * [Angular-Fork](https://angular.io/)

### Commercial Support

### Crear Base de Datos en postgres

  - `sudo su postgres`
  - `psql -c "DROP DATABASE devlaravel_app"`
  - `psql -c "DROP USER devlaravel_user"`
  - `psql -c "CREATE USER devlaravel_user WITH ENCRYPTED PASSWORD '12233'"`
  - `psql -c "CREATE DATABASE devlaravel_app WITH OWNER devlaravel_user"`


### Puede instalar dependencias con make deps 


DevLaravel es soportado por [@alfredynho](alfredynho.cg@gmail.com) && [@Luis](luisangelql10@gmail.com) .

<p align="center"><img src="https://goo.gl/Mi3erN"></p>
