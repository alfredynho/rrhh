<?php
use Illuminate\Support\Facades\Route;

Route::get('/recursos-humanos', function () {
    return view('content/content');
})->name('main');

// https://github.com/beyondcode/laravel-dump-server

// Routes Contadores
Route::get('/cantidad','DashboardController@index');

Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


// Routes Category
Route::get('/unidad','UnidadController@index');
Route::post('/unidad/registrar','UnidadController@store');
Route::put('/unidad/actualizar','UnidadController@update');
Route::put('/unidad/desactivar','UnidadController@desactivar');
Route::put('/unidad/activar','UnidadController@activar');
Route::put('/unidad/eliminar','UnidadController@eliminar');
Route::get('/unidades-inso/selectUnidad', 'UnidadController@selectUnidad');
Route::get('unidad/unidadesPdf','UnidadController@unidadesPdf')->name('unidades_pdf');

// Route::apiResource('/calendar', 'CalendarController');

Route::get('/calendario','CalendarioController@index');
Route::post('/calendario/registrar','CalendarioController@store');
Route::put('/calendario/actualizar','CalendarioController@update');
Route::put('/calendario/desactivar','CalendarioController@desactivar');
Route::put('/calendario/activar','CalendarioController@activar');
Route::put('/calendario/eliminar','CalendarioController@eliminar');
Route::get('/calendario-inso/selectUnidad', 'CalendarioController@selectUnidad');
// Route::get('caledario/unidadesPdf','CalendarioController@unidadesPdf')->name('unidades_pdf');


// Routes Category
Route::get('/profesion','ProfesionesController@index');
Route::post('/profesion/registrar','ProfesionesController@store');
Route::put('/profesion/actualizar','ProfesionesController@update');
Route::put('/profesion/desactivar','ProfesionesController@desactivar');
Route::put('/profesion/activar','ProfesionesController@activar');
Route::put('/profesion/eliminar','ProfesionesController@eliminar');
Route::get('/profesion/selectProfesion', 'ProfesionesController@selectProfesion');


// Routes Personal
Route::get('/personal','PersonalController@index');
Route::post('/personal/registrar','PersonalController@store');
Route::put('/personal/actualizar','PersonalController@update');
Route::put('/personal/desactivar','PersonalController@desactivar');
Route::put('/personal/activar','PersonalController@activar');
Route::put('/personal/eliminar','PersonalController@eliminar');
Route::get('/personal/selectPersonal', 'PersonalController@selectPersonal');
Route::get('/personal/personalPdf/{id}','PersonalController@personalPdf')->name('personal_pdf');
Route::get('/personal_listado/personalPdf','PersonalController@personalListadoPdf')->name('personal_listado_pdf');


// Url para detalle de personal
Route::get('/personal/detalle/{id}','ProfileController@personal_detalle')->name('personal_detalle');

// cargo
Route::get('/cargo','CargoController@index');
Route::post('/cargo/registrar','CargoController@store');
Route::put('/cargo/actualizar','CargoController@update');
Route::put('/cargo/desactivar','CargoController@desactivar');
Route::put('/cargo/activar','CargoController@activar');
Route::put('/cargo/eliminar','CargoController@eliminar');
Route::get('/cargo/selectCargo', 'CargoController@selectCargo');

// Routes Client
Route::get('/cliente','ClientController@index');
Route::post('/cliente/registrar','ClientController@store');
Route::put('/cliente/actualizar','ClientController@update');
Route::put('/cliente/desactivar','ClientController@desactivar');
Route::put('/cliente/activar','ClientController@activar');
Route::get('/cliente/selectClient', 'ClientController@selectClient');

// Routes Categorias INSO
Route::get('/status','StatusController@index');
Route::post('/status/registrar','StatusController@store');
Route::put('/status/actualizar','StatusController@update');
Route::put('/status/desactivar','StatusController@desactivar');
Route::put('/status/activar','StatusController@activar');
Route::put('/status/eliminar','StatusController@eliminar');
Route::get('/status/selectStatus', 'StatusController@selectStatus');

// Route Person
Route::get('/persona','ProviderController@index');

// urls para accidentes de trabajo

// Routes Razon Social
Route::get('/razon_social','RazonSocialController@index');
Route::post('/razon_social/registrar','RazonSocialController@store');
Route::put('/razon_social/actualizar','RazonSocialController@update');
Route::put('/razon_social/desactivar','RazonSocialController@desactivar');
Route::put('/razon_social/activar','RazonSocialController@activar');
Route::put('/razon_social/eliminar','RazonSocialController@eliminar');
Route::get('/razon_social/selectCategoria', 'RazonSocialController@selectRazon');

// Routes Afps
Route::get('/afps','AfpsController@index');
Route::post('/afps/registrar','AfpsController@store');
Route::put('/afps/actualizar','AfpsController@update');
Route::put('/afps/desactivar','AfpsController@desactivar');
Route::put('/afps/activar','AfpsController@activar');
Route::put('/afps/eliminar','AfpsController@eliminar');
Route::get('/afps/selectCategoria', 'AfpsController@selectRazon');

// Routes Grupos
Route::get('/grupo','GruposController@index');
Route::post('/grupo/registrar','GruposController@store');
Route::put('/grupo/actualizar','GruposController@update');
Route::put('/grupo/desactivar','GruposController@desactivar');
Route::put('/grupo/activar','GruposController@activar');
Route::put('/grupo/eliminar','GruposController@eliminar');
Route::get('/grupo/selectGrupo', 'GruposController@selectGrupo');

// Routes Ocupaciones
Route::get('/ocupaciones','OcupacionesController@index');
Route::post('/ocupaciones/registrar','OcupacionesController@store');
Route::put('/ocupaciones/actualizar','OcupacionesController@update');
Route::put('/ocupaciones/desactivar','OcupacionesController@desactivar');
Route::put('/ocupaciones/activar','OcupacionesController@activar');
Route::put('/ocupaciones/eliminar','OcupacionesController@eliminar');
Route::get('/ocupaciones/selectOcupacion', 'OcupacionesController@selectOcupacion');

// Routes accidentes
Route::get('/accidentes','AccidentesController@index');
Route::post('/accidentes/registrar','AccidentesController@store');
Route::put('/accidentes/actualizar','AccidentesController@update');
Route::put('/accidentes/desactivar','AccidentesController@desactivar');
Route::put('/accidentes/activar','AccidentesController@activar');
Route::put('/accidentes/eliminar','AccidentesController@eliminar');
Route::get('/accidentes/selectUnits', 'AccidentesController@selectUnits');

// Listado de Roles
Route::get('/rol', 'RolController@index');

// Listado de usuarios
Route::get('/usuarios', 'UserController@index');
Route::post('/usuarios/registrar', 'UserController@store');
Route::put('/usuarios/actualizar', 'UserController@update');
Route::put('/usuarios/desactivar', 'UserController@desactivar');
Route::put('/usuarios/activar', 'UserController@activar');


Route::get('categoria/listarPdf','CategoryController@listarPdf')->name('reporte_pdf');
Route::get('vacaciones/vacacionesPdf','CategoryController@vacacionesPdf')->name('vacaciones_pdf');

// Solicitud de 2 horas
Route::get('/horas','SolicitudesController@index');
Route::post('/horas/registrar','SolicitudesController@store');
Route::post('/vacaciones/registrar','SolicitudesController@storeVacaciones');
Route::put('/horas/actualizar','SolicitudesController@update');
Route::put('/horas/desactivar','SolicitudesController@desactivar');
Route::put('/horas/activar','SolicitudesController@activar');
Route::put('/horas/eliminar','SolicitudesController@eliminar');
Route::get('/horas/selectCargo', 'SolicitudesController@selectCargo');

Route::get('/','Auth\LoginController@showLoginForm');
Route::post('/login','Auth\LoginController@login')->name('login');
Route::get('/home', 'HomeController@index')->name('home');
