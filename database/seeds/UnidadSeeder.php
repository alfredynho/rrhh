<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Unidad;
use Carbon\Carbon;

class UnidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(storage_path('unidades.csv'), "r");
        $cont = 1;
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Unidad::create([
                'id' => $cont,
                'codigo' => $csvLine[0],
                'nombre' => $csvLine[1],
                'estado' => $csvLine[2],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        $cont = $cont + 1;
        }
    }
}
