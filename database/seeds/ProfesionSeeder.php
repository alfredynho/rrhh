<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\Profesion;
use Carbon\Carbon;

class ProfesionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(storage_path('profesiones.csv'), "r");
        $cont = 1;
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            Profesion::create([
                'id' => $cont,
                'nombre' => $csvLine[0],
                'estado' => $csvLine[1],
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);

        $cont = $cont + 1;
        }
    }
}
