<?php

use Database\Seeders\ProfesionSeeder;
use Database\Seeders\UnidadSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            // UnidadSeeder::class,
            ProfesionSeeder::class,
        ]);
    }
}
