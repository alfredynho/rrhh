<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('institucion');
            $table->string('mencion');
            $table->string('grado');
            $table->string('profesion');

            $table->timestamps();

            $table->integer('id_personal')->unsigned();
            $table->foreign('id_personal')->references('id')->on('personal')->onDelete('cascade');
        });

        Schema::create('experiencia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('institucion');
            $table->date('periodo_inicio');
            $table->date('periodo_fin');
            $table->string('cargo');
            $table->string('modalidad'); // Planta, Eventual, consultor

            $table->timestamps();

            $table->integer('id_personal')->unsigned();
            $table->foreign('id_personal')->references('id')->on('personal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
        Schema::dropIfExists('experiencia');
    }
}
