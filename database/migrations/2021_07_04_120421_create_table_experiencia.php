<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableExperiencia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_experiencia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entidad');
            $table->string('pais');
            $table->string('cargo');

            $table->date('desde');
            $table->date('hasta');

            $table->integer('id_capacitacion')->unsigned();
            $table->foreign('id_capacitacion')->references('id')->on('personal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_experiencia');
    }
}
