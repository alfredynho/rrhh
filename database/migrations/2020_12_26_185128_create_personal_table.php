<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cedula')->index()->unique();
            $table->string('nombres');
            $table->string('paterno')->nullable();
            $table->string('materno')->nullable();
            $table->date('fecha_nacimiento');
            $table->string('caja')->nullable();
            $table->string('slug');
            $table->string('tipo_personal'); // Refiere a si es de Planta. Eventual, Consultor en Linea o compra de servicio.
            $table->integer('id_cargo')->unsigned();
            $table->foreign('id_cargo')->references('id')->on('cargo')->onDelete('cascade');

            $table->integer('id_profesion')->unsigned();
            $table->foreign('id_profesion')->references('id')->on('profesiones')->onDelete('cascade');
            $table->string('correo')->nullable();
            $table->string('nacionalidad')->nullable();
            $table->date('fecha_ingreso');
            $table->boolean('estado_civil')->default(0);
            $table->string('celular')->nullable();
            $table->boolean('genero')->default(1);
            $table->longText('direccion');
            $table->boolean('afp')->default(1);
            $table->integer('edad');
            $table->boolean('seguro')->default(1);
            $table->boolean('estado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal');
    }
}
