<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVacacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('razon'); // CNS // 2 HORAS // COMISION // VACACIONES
            $table->integer('personal_id')->unsigned();
            $table->foreign('personal_id')->references('id')->on('personal');
            $table->integer('cantidad_dias')->nullable();
            $table->date('fecha_inicio')->nullable(); // para vacaciones
            $table->date('fecha_finalizacion')->nullable(); // para vacaciones
            $table->string('descripcion');
            $table->integer('saldo_horas')->nullable(); // saldo horas
            $table->integer('saldo_vacaciones')->nullable(); // saldo vacaciones
            $table->integer('hora')->nullable(); // hora que saco
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
