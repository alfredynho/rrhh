<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCapacitacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_capacitacion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nivel');
            $table->string('institucion');

            $table->date('desde');
            $table->date('hasta');

            $table->string('situacion'); //EGRESADO, DIPLOMADO, ETC
            $table->integer('id_capacitacion')->unsigned();
            $table->foreign('id_capacitacion')->references('id')->on('personal')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_capacitacion');
    }
}
