<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCapacitacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('capacitaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->string('institucion');
            $table->boolean('modalidad')->default(1);
            $table->date('fecha_realizado');
            $table->string('carga_horario');

            $table->timestamps();

            $table->integer('id_personal')->unsigned();
            $table->foreign('id_personal')->references('id')->on('personal')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('capacitaciones');
    }
}
