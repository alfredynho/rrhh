-- MySQL dump 10.19  Distrib 10.3.29-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: rrhhvue
-- ------------------------------------------------------
-- Server version	10.3.29-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `afp`
--

DROP TABLE IF EXISTS `afp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `afp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `afp`
--

LOCK TABLES `afp` WRITE;
/*!40000 ALTER TABLE `afp` DISABLE KEYS */;
/*!40000 ALTER TABLE `afp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ausencias`
--

DROP TABLE IF EXISTS `ausencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ausencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `personal_id` bigint(20) unsigned NOT NULL,
  `fecha_ausencia` date NOT NULL,
  `justificado` tinyint(1) NOT NULL,
  `observaciones` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ausencias_personal_id_foreign` (`personal_id`),
  CONSTRAINT `ausencias_personal_id_foreign` FOREIGN KEY (`personal_id`) REFERENCES `personal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ausencias`
--

LOCK TABLES `ausencias` WRITE;
/*!40000 ALTER TABLE `ausencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `ausencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendario`
--

DROP TABLE IF EXISTS `calendario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendario` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_actividad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendario`
--

LOCK TABLES `calendario` WRITE;
/*!40000 ALTER TABLE `calendario` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendars`
--

DROP TABLE IF EXISTS `calendars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendars` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_published` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendars`
--

LOCK TABLES `calendars` WRITE;
/*!40000 ALTER TABLE `calendars` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capacitaciones`
--

DROP TABLE IF EXISTS `capacitaciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `capacitaciones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `institucion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modalidad` tinyint(1) NOT NULL DEFAULT 1,
  `fecha_realizado` date NOT NULL,
  `carga_horario` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_personal` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `capacitaciones_id_personal_foreign` (`id_personal`),
  CONSTRAINT `capacitaciones_id_personal_foreign` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capacitaciones`
--

LOCK TABLES `capacitaciones` WRITE;
/*!40000 ALTER TABLE `capacitaciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `capacitaciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cargo`
--

DROP TABLE IF EXISTS `cargo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `id_unidad` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cargo_id_unidad_foreign` (`id_unidad`),
  CONSTRAINT `cargo_id_unidad_foreign` FOREIGN KEY (`id_unidad`) REFERENCES `unidad` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo`
--

LOCK TABLES `cargo` WRITE;
/*!40000 ALTER TABLE `cargo` DISABLE KEYS */;
INSERT INTO `cargo` VALUES (1,'JEFE DE LA UNIDAD DE ADMINISTRACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(2,'SOPORTE TECNICO EN SISTEMAS',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(3,'RESPONSABLE DE ALMACENES',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(4,'RESPONSABLE DE ESTADISTICA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(5,'RESPONSABLE DE ACTIVOS FIJOS Y ALMACENES',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(6,'TRABAJADOR MANUAL I',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(7,'RESPONSABLE DE MANTENIMIENTO GENERAL',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(8,'RESPONSABLE DE CONTRATACIONES',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(9,'AUXILIAR ADMINISTRATIVO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(10,'TRABAJADORA MANUAL II',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(11,'ENCARGADA DE TESORERIA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(12,'RESPONSABLE DE SISTEMAS DE LA INFORMACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(13,'RESPONSABLE DEL AREA DE INFORMATICA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(14,'PLANIFICADORA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(15,'AUXILIAR DE ESTADISTICA II',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(16,'AUXILIAR TECNICO ADMINISTRATIVO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(17,'AUXILIAR ADMINISTRATIVO II',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(18,'APOYO ADMINISTRATIVO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(19,'RECURSOS HUMANOS',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(20,'RESPONSABLE DE CONTRATACIONES A I',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(21,'SECRETARIA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(22,'AUXILIAR DE MANTENIMIENTO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(23,'RESPONSABLE DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(24,'RESPONSABLE DEL CENTRO DE CAPACITACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(25,'JEFE DE LA UNIDAD TECNICA DE HIGIENE Y SEGURIDAD INDUSTRIAL',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(26,'SECRETARIA DE UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(27,'INGENIERA INDUSTRIAL UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(28,'QUIMICO UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(29,'TECNICO UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(30,'QUIMICO UTHSIMA II',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(31,'INGENIERO UTHSIMA II',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(32,'FACILITADOR DE CURSO UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(33,'SECRETARIA DE UTSHIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(34,'JEFE DE LA UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(35,'AUXILIAR TECNICO I UNIDAD TECNICA DE MEDICINA DEL TRABAJO ',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(36,'AUXILIAR TECNICO II UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(37,'BIOQUIMICO I',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(38,'CONSULTOR DE LINEA MEDICO GENERAL SANTA CRUZ',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(39,'BIOQUIMICO II ',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(40,'PROFESIONAL MEDICO I UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(41,'PROFESIONAL MEDICO II UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(42,'PROFESIONAL ODONTOLOGO UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(43,'PROFESIONAL MEDICO IV UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(44,'PROFESIONAL MEDICO INSO REGIONAL COCHABAMBA',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(45,'PROFESIONAL MEDICO III UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(46,'BIOTECNOLOGA',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(47,'PROFESIONAL MEDICO V UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(48,'RESPONSABLE DE RADIOLOGIA',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(49,'TECNICO DE RAYOS X',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(50,'SECRETARIA Y ENLACE ADMINISTRATIVO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(51,'RESPONSABLE DE INVESTIGACION',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(52,'TECNICO EN LABORATORIO CLINICO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(53,'FONOAUDIOLOGO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(54,'AUXILIAR EN ENFERMERIA II',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(55,'AUXILIAR EN ENFERMERIA I',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(56,'ASISTENTE ADMINISTRATIVO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(57,'PROFESIONAL MEDICO VI UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20');
/*!40000 ALTER TABLE `cargo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contratos`
--

DROP TABLE IF EXISTS `contratos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contratos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contratos`
--

LOCK TABLES `contratos` WRITE;
/*!40000 ALTER TABLE `contratos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contratos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudios`
--

DROP TABLE IF EXISTS `estudios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estudios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mencion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grado` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profesion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_personal` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `estudios_id_personal_foreign` (`id_personal`),
  CONSTRAINT `estudios_id_personal_foreign` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudios`
--

LOCK TABLES `estudios` WRITE;
/*!40000 ALTER TABLE `estudios` DISABLE KEYS */;
/*!40000 ALTER TABLE `estudios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiencia`
--

DROP TABLE IF EXISTS `experiencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `experiencia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `institucion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `periodo_inicio` date NOT NULL,
  `periodo_fin` date NOT NULL,
  `cargo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `modalidad` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_personal` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `experiencia_id_personal_foreign` (`id_personal`),
  CONSTRAINT `experiencia_id_personal_foreign` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiencia`
--

LOCK TABLES `experiencia` WRITE;
/*!40000 ALTER TABLE `experiencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `experiencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (46,'2014_10_12_000000_create_users_table',1),(47,'2014_10_12_100000_create_password_resets_table',1),(48,'2019_10_29_212415_create_calendars_table',1),(49,'2020_12_08_230150_create_unidad_table',1),(50,'2020_12_08_230839_create_profesiones_table',1),(51,'2020_12_08_233041_create_contratos_table',1),(52,'2020_12_16_011302_create_cargo_table',1),(53,'2020_12_26_183832_create_afp_table',1),(54,'2020_12_26_185128_create_personal_table',1),(55,'2021_01_24_205507_create_capacitaciones_table',1),(56,'2021_01_24_211116_create_salidas_table',1),(57,'2021_01_24_212025_create_ausencias_table',1),(61,'2021_01_24_213108_create_vacaciones_table',2),(62,'2021_02_03_003626_create_estudios_table',2),(65,'2021_04_25_083823_create_calendario_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal`
--

DROP TABLE IF EXISTS `personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cedula` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombres` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paterno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `materno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_nacimiento` date NOT NULL,
  `caja` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_cargo` int(10) unsigned NOT NULL,
  `id_profesion` int(10) unsigned NOT NULL,
  `correo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fecha_ingreso` date NOT NULL,
  `estado_civil` tinyint(1) NOT NULL DEFAULT 0,
  `celular` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `genero` tinyint(1) NOT NULL DEFAULT 1,
  `direccion` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `afp` tinyint(1) NOT NULL DEFAULT 1,
  `edad` int(11) NOT NULL,
  `seguro` tinyint(1) NOT NULL DEFAULT 1,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_cedula_unique` (`cedula`),
  KEY `personal_id_cargo_foreign` (`id_cargo`),
  KEY `personal_id_profesion_foreign` (`id_profesion`),
  CONSTRAINT `personal_id_cargo_foreign` FOREIGN KEY (`id_cargo`) REFERENCES `cargo` (`id`) ON DELETE CASCADE,
  CONSTRAINT `personal_id_profesion_foreign` FOREIGN KEY (`id_profesion`) REFERENCES `profesiones` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal`
--

LOCK TABLES `personal` WRITE;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` VALUES (1,'0202020','ALFREDO','CALLIZAYA','GUTIERREZ','1991-05-22','','alfredogutierrezcallizaya',9,1,'dasd','2021-05-20',2,NULL,1,'sadlasldlsad',1,28,1,1,'2021-05-22 16:18:37','2021-05-22 16:18:37'),(2,'4277300','JAIME','PEREYRA','FLORES','2021-12-31','','jaimepereyraflores',18,1,'a@mail.com','2021-12-31',1,NULL,1,'san pedro',1,2,1,1,'2021-06-21 23:21:40','2021-06-21 23:21:40');
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profesiones`
--

DROP TABLE IF EXISTS `profesiones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profesiones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profesiones`
--

LOCK TABLES `profesiones` WRITE;
/*!40000 ALTER TABLE `profesiones` DISABLE KEYS */;
INSERT INTO `profesiones` VALUES (1,'Abogado',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(2,'Medico cirujano',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(3,'Ingeniero',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(4,'Historiador',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(5,'Biologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(6,'Filologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(7,'Matematico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(8,'Arquitecto',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(9,'Profesor',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(10,'Periodista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(11,'Fisico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(12,'Sociologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(13,'Quimico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(14,'Politologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(15,'Electricista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(16,'Bibliotecologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(17,'Técnico de sonido',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(18,'Archivologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(19,'Filosofo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(20,'Secretaria',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(21,'Antropologo	',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(22,'Tecnico en turismo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(23,'Administrador',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(24,'Linguista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(25,'Contador',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(26,'Psicoanalista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(27,'Arqueologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(28,'Enfermero',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(29,'Paleontologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(30,'Paramedico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(31,'Geografo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(32,'Musico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(33,'Psicologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(34,'Traductor',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(35,'Computista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(36,'Economista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(37,'Botanico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(38,'Radiologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(39,'Farmacologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(40,'Ecologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33');
/*!40000 ALTER TABLE `profesiones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salidas`
--

DROP TABLE IF EXISTS `salidas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `salidas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cantidad_horas` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `personal` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `salidas_personal_foreign` (`personal`),
  CONSTRAINT `salidas_personal_foreign` FOREIGN KEY (`personal`) REFERENCES `personal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salidas`
--

LOCK TABLES `salidas` WRITE;
/*!40000 ALTER TABLE `salidas` DISABLE KEYS */;
/*!40000 ALTER TABLE `salidas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `solicitudes`
--

DROP TABLE IF EXISTS `solicitudes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitudes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `razon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_id` bigint(20) unsigned NOT NULL,
  `cantidad_dias` int(11) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_finalizacion` date DEFAULT NULL,
  `descripcion` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `saldo_horas` int(11) DEFAULT NULL,
  `saldo_vacaciones` int(11) DEFAULT NULL,
  `hora` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `solicitudes_personal_id_foreign` (`personal_id`),
  CONSTRAINT `solicitudes_personal_id_foreign` FOREIGN KEY (`personal_id`) REFERENCES `personal` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitudes`
--

LOCK TABLES `solicitudes` WRITE;
/*!40000 ALTER TABLE `solicitudes` DISABLE KEYS */;
INSERT INTO `solicitudes` VALUES (1,'1',1,NULL,NULL,NULL,'',NULL,NULL,1,'2021-06-03 14:52:08','2021-06-03 14:52:08',NULL),(2,'1',1,NULL,NULL,NULL,'',NULL,NULL,1,'2021-06-03 14:53:25','2021-06-03 14:53:25',NULL),(3,'1',1,NULL,NULL,NULL,'',NULL,NULL,2,'2021-06-22 09:57:22','2021-06-22 09:57:22',NULL),(4,'2',2,NULL,NULL,NULL,'',NULL,NULL,1,'2021-06-22 10:21:46','2021-06-22 10:21:46',NULL),(5,'1',1,NULL,NULL,NULL,'',NULL,NULL,1,'2021-06-22 10:28:42','2021-06-22 10:28:42',NULL),(6,'1',1,NULL,'2021-06-22','2021-06-26','',NULL,NULL,NULL,'2021-06-22 10:28:58','2021-06-22 10:28:58',NULL),(7,'1',1,NULL,NULL,NULL,'',NULL,NULL,1,'2021-06-22 10:56:33','2021-06-22 10:56:33',NULL),(8,'1',1,NULL,'2021-06-16','2021-06-26','',NULL,NULL,NULL,'2021-06-22 10:56:48','2021-06-22 10:56:48',NULL),(9,'1',1,NULL,NULL,NULL,'',NULL,NULL,1,'2021-06-26 23:29:16','2021-06-26 23:29:16',NULL);
/*!40000 ALTER TABLE `solicitudes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unidad`
--

DROP TABLE IF EXISTS `unidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unidad` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unidad`
--

LOCK TABLES `unidad` WRITE;
/*!40000 ALTER TABLE `unidad` DISABLE KEYS */;
INSERT INTO `unidad` VALUES (1,'UADMIN','UNIDAD DE ASUNTOS ADMINISTRATIVOS',1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(2,'UTMT','UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(3,'UTSHIMA','UNIDAD TECNICA DE HIGIENE SEGURIDAD INDUSTRIAL Y MEDIO AMBIENTE',1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(4,'INSO','INSTITUTO NACIONAL DE SALUD OCUPACIONAL',1,'2021-02-03 18:04:20','2021-02-03 18:04:20');
/*!40000 ALTER TABLE `unidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `paterno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `materno` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bloqueado` tinyint(1) NOT NULL DEFAULT 0,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-27  9:27:13



INSERT INTO `profesiones` VALUES (1,'Abogado',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(2,'Medico cirujano',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(3,'Ingeniero',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(4,'Historiador',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(5,'Biologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(6,'Filologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(7,'Matematico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(8,'Arquitecto',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(9,'Profesor',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(10,'Periodista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(11,'Fisico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(12,'Sociologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(13,'Quimico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(14,'Politologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(15,'Electricista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(16,'Bibliotecologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(17,'Técnico de sonido',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(18,'Archivologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(19,'Filosofo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(20,'Secretaria',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(21,'Antropologo	',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(22,'Tecnico en turismo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(23,'Administrador',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(24,'Linguista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(25,'Contador',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(26,'Psicoanalista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(27,'Arqueologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(28,'Enfermero',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(29,'Paleontologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(30,'Paramedico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(31,'Geografo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(32,'Musico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(33,'Psicologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(34,'Traductor',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(35,'Computista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(36,'Economista',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(37,'Botanico',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(38,'Radiologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(39,'Farmacologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33'),(40,'Ecologo',1,'2021-05-22 16:16:33','2021-05-22 16:16:33');

INSERT INTO `unidad` VALUES (1,'UADMIN','UNIDAD DE ASUNTOS ADMINISTRATIVOS',1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(2,'UTMT','UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(3,'UTSHIMA','UNIDAD TECNICA DE HIGIENE SEGURIDAD INDUSTRIAL Y MEDIO AMBIENTE',1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(4,'INSO','INSTITUTO NACIONAL DE SALUD OCUPACIONAL',1,'2021-02-03 18:04:20','2021-02-03 18:04:20');

INSERT INTO `cargo` VALUES (1,'JEFE DE LA UNIDAD DE ADMINISTRACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(2,'SOPORTE TECNICO EN SISTEMAS',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(3,'RESPONSABLE DE ALMACENES',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(4,'RESPONSABLE DE ESTADISTICA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(5,'RESPONSABLE DE ACTIVOS FIJOS Y ALMACENES',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(6,'TRABAJADOR MANUAL I',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(7,'RESPONSABLE DE MANTENIMIENTO GENERAL',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(8,'RESPONSABLE DE CONTRATACIONES',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(9,'AUXILIAR ADMINISTRATIVO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(10,'TRABAJADORA MANUAL II',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(11,'ENCARGADA DE TESORERIA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(12,'RESPONSABLE DE SISTEMAS DE LA INFORMACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(13,'RESPONSABLE DEL AREA DE INFORMATICA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(14,'PLANIFICADORA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(15,'AUXILIAR DE ESTADISTICA II',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(16,'AUXILIAR TECNICO ADMINISTRATIVO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(17,'AUXILIAR ADMINISTRATIVO II',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(18,'APOYO ADMINISTRATIVO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(19,'RECURSOS HUMANOS',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(20,'RESPONSABLE DE CONTRATACIONES A I',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(21,'SECRETARIA',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(22,'AUXILIAR DE MANTENIMIENTO',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(23,'RESPONSABLE DE TECNOLOGIAS DE LA INFORMACION Y COMUNICACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(24,'RESPONSABLE DEL CENTRO DE CAPACITACION',1,1,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(25,'JEFE DE LA UNIDAD TECNICA DE HIGIENE Y SEGURIDAD INDUSTRIAL',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(26,'SECRETARIA DE UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(27,'INGENIERA INDUSTRIAL UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(28,'QUIMICO UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(29,'TECNICO UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(30,'QUIMICO UTHSIMA II',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(31,'INGENIERO UTHSIMA II',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(32,'FACILITADOR DE CURSO UTHSIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(33,'SECRETARIA DE UTSHIMA',1,3,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(34,'JEFE DE LA UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(35,'AUXILIAR TECNICO I UNIDAD TECNICA DE MEDICINA DEL TRABAJO ',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(36,'AUXILIAR TECNICO II UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(37,'BIOQUIMICO I',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(38,'CONSULTOR DE LINEA MEDICO GENERAL SANTA CRUZ',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(39,'BIOQUIMICO II ',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(40,'PROFESIONAL MEDICO I UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(41,'PROFESIONAL MEDICO II UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(42,'PROFESIONAL ODONTOLOGO UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(43,'PROFESIONAL MEDICO IV UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(44,'PROFESIONAL MEDICO INSO REGIONAL COCHABAMBA',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(45,'PROFESIONAL MEDICO III UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(46,'BIOTECNOLOGA',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(47,'PROFESIONAL MEDICO V UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(48,'RESPONSABLE DE RADIOLOGIA',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(49,'TECNICO DE RAYOS X',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(50,'SECRETARIA Y ENLACE ADMINISTRATIVO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(51,'RESPONSABLE DE INVESTIGACION',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(52,'TECNICO EN LABORATORIO CLINICO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(53,'FONOAUDIOLOGO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(54,'AUXILIAR EN ENFERMERIA II',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(55,'AUXILIAR EN ENFERMERIA I',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(56,'ASISTENTE ADMINISTRATIVO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20'),(57,'PROFESIONAL MEDICO VI UNIDAD TECNICA DE MEDICINA DEL TRABAJO',1,2,'2021-02-03 18:04:20','2021-02-03 18:04:20');


INSERT INTO `personal` VALUES (1,'4277301','JAIME ALVARO','PEREYRA','FLORES','2021-12-31','','jaimepereyraflores',2,18,1,'a@mail.com','Boliviano','2021-12-31',1,NULL,1,'san pedro',1,2,1,1,'2021-06-21 23:21:40','2021-06-21 23:21:40');
