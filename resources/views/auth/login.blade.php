@extends('auth.contenido')

@section('login')
<section class="animated bounceInLeft height-100vh d-flex align-items-center page-section-ptb login" style="background-image: url(images/login.png);" >
    <div class="container">
    <form action="{{ route('login') }}" method="POST">
        {{ csrf_field() }}
      <div class="row justify-content-center no-gutters vertical-align">
         <div class="col-lg-4 col-md-6 login-fancy-bg bg" style="background-image: url(images/login-inner-bg.png);">
           <div class="login-fancy">
            <h3 class="text-white mb-20">SISTEMA DE RECURSOS HUMANOS</h3>
            <ul class="list-unstyled  pos-bot pb-30">
                <li class="list-inline-item"><a class="text-white" href="#"> Desarrollador por Jaime Flores Pereyra</a> </li>
              </ul>
           </div>
         </div>
         <div class="col-lg-4 col-md-6 bg-white">
          <div class="login-fancy pb-40 clearfix">
              <img src="images/asistente.gif" width="90" height="90">
              <h3 class="mb-30 typewriter">INICIO DE SESIÓN</h3>
              <div class="section-field mb-20{{$errors->has('usuario' ? 'is-invalid' : '')}}">
               <label class="mb-10" for="name">Usuario* </label>
                <input type="text" value="{{old('usuario')}}" name="usuario" id="usuario" class="web form-control" placeholder="Usuario">
                 {!!$errors->first('usuario','<span class="invalid-feedback">:message</span>')!!}
              </div>
              <div class="section-field mb-20{{$errors->has('password' ? 'is-invalid' : '')}}">
               <label class="mb-10" for="Password">Contraseña* </label>
                   <input type="password" name="password" id="password" class="Password form-control" placeholder="Password">
                {!!$errors->first('password','<span class="invalid-feedback">:message</span>')!!}

              </div>
                <button type="submit" class="button">
                  <span>INGRESAR</span>
                  <i class="fa fa-check"></i>
               </button>
            </div>
          </div>
        </div>
    </form>
    </div>
  </section>

@endsection('login')
