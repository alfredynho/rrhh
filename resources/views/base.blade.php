<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="INSO, Instituto Nacional de Salud Ocupacional" />
<meta name="description" content="Sistema de Administracion INSO" />
<meta name="author" content="INSO - SISTEMA DE RECURSOS HUMANOS" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<title>{{ config('app.name') }}</title>

<link rel="shortcut icon" href="images/favicon.png" />
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900">
<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="css/animate.css" />
<link rel="stylesheet" type="text/css" href="css/iziToast.min.css" />
<meta name="csrf-token" content="{{ csrf_token() }}">

</head>

<body>

<div id="app" class="wrapper">

<div id="pre-loader">
    <img src="images/pre-loader/loader-01.svg" alt="">
</div>

  @include('includes.nav')

<div class="container-fluid">
  <div class="row">

    @if(Auth::check())
        @if (Auth::user()->idrol == 1)
            @include('includes.menu-admin')
        @elseif (Auth::user()->idrol == 2)
            @include('includes.menu-operador')
        @elseif (Auth::user()->idrol == 3)
            @include('includes.menu-personal')
        @else

        @endif

    @endif

    <div class="content-wrapper">
      @yield('content')

    @include('includes.footer')

    <script src="js/app.js"></script>
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/plugins-jquery.js"></script>
    <script>var plugin_path = 'js/';</script>
    <script src="js/chart-init.js"></script>
    <script src="js/calendar.init.js"></script>
    <script src="js/sparkline.init.js"></script>
    <script src="js/morris.init.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/sweetalert2.js"></script>
    <script src="js/iziToast.min.js"></script>
    <script src="js/validation.js"></script>
    <script src="js/lobilist.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/vcalendar.js"></script>

</body>
</html>
