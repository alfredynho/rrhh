<footer class="bg-white p-4">
    <div class="row">
      <div class="col-md-6">
        <div class="text-center text-md-left">
            <p class="mb-0"> &copy; Sistema  <span id="copyright"> </span>. <a href="#"> INSO </a> {{ date('Y') }} </p>
        </div>
      </div>
      <div class="col-md-6">
        <ul class="text-center text-md-right">
          <li class="list-inline-item"><a href="#">INSO {{ now()->year }} DESARROLLADO POR JAIME FLORES PEREYRA</a> </li>
        </ul>
      </div>
    </div>
  </footer>
  </div>
</div>
</div>
</div>
