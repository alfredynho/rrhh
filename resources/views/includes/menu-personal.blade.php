<div class="side-menu-fixed"> {{-- light-side-menu --}}
    <div class="scrollbar side-menu-bg">
     <ul class="nav navbar-nav side-menu" id="sidebarnav">
       <li @click="menu=0">
           <a href="#"><img src="{{ asset('images/home.png') }}"> &nbsp;<span class="right-nav-text">Dashboard</span> </a>
       </li>

        <li @click="menu=11">
            <a href="#"><img src="{{ asset('images/calendar.png') }}"> &nbsp;<span class="right-nav-text">Mi Calendario</span> </a>
        </li>

        <li @click="menu=1">
            <a href="#"><img src="{{ asset('images/bookmark.png') }}"> &nbsp; <span class="right-nav-text">Unidad</span> </a>
        </li>

        <li @click="menu=2">
            <a href="#"><img src="{{ asset('images/work.png') }}"> &nbsp;<span class="right-nav-text">Profesiones</span> </a>
        </li>

        {{-- <li @click="menu=2">
            <a href="#"><i class="ti-file"></i><span class="right-nav-text">Contratos</span> </a>
        </li> --}}

        <li @click="menu=3">
            <a href="#"><img src="{{ asset('images/cargo.png') }}"> &nbsp;<span class="right-nav-text">Cargo</span> </a>
        </li>

        <li @click="menu=4">
            <a href="#"><img src="{{ asset('images/personal.png') }}"> &nbsp;<span class="right-nav-text">Personal</span> </a>
        </li>

        <li @click="menu=5">
            <a href="#"><img src="{{ asset('images/vacaciones.png') }}"> &nbsp;<span class="right-nav-text">Solicitudes</span> </a>
        </li>

        <li @click="menu=6">
            <a href="#"><img src="{{ asset('images/capacitacion.png') }}"> &nbsp;<span class="right-nav-text">Capacitaciones</span> </a>
        </li>

        <li @click="menu=7">
            <a href="#"><img src="{{ asset('images/salidas.png') }}"> &nbsp;<span class="right-nav-text">Salidas</span> </a>
        </li>

        <li @click="menu=8">
            <a href="#"><img src="{{ asset('images/ausencias.png') }}"> &nbsp;<span class="right-nav-text">Ausencias</span> </a>
        </li>

        <li @click="menu=10">
            <a href="#"><img src="{{ asset('images/estudios.png') }}"> &nbsp;<span class="right-nav-text">Estudios</span> </a>
        </li>

        <li @click="menu=13">
            <a href="#"><img src="{{ asset('images/estudios.png') }}"> &nbsp;<span class="right-nav-text">Asignacion</span> </a>
        </li>


         {{-- <li>
          <a href="#" data-toggle="collapse" data-target="#proveedores">
            <div class="pull-left"><i class="ti-id-badge"></i><span class="right-nav-text">Proveedores INSO</span></div>
            <div class="pull-right"><i class="ti-plus"></i></div><div class="clearfix"></div>
          </a>
          <ul id="proveedores" class="collapse" data-parent="#sidebarnav">
            <li @click="menu=1"> <a href="#">Categoria</a> </li>
            <li @click="menu=2"> <a href="#">Clientes</a> </li>
            <li @click="menu=3"> <a href="#">Proveedores</a> </li>
          </ul>
        </li>

         <li>
          <a href="#" data-toggle="collapse" data-target="#centro-inso">
            <div class="pull-left"><i class="ti-desktop"></i><span class="right-nav-text">Centro INSO</span></div>
            <div class="pull-right"><i class="ti-plus"></i></div><div class="clearfix"></div>
          </a>
          <ul id="centro-inso" class="collapse" data-parent="#sidebarnav">
            <li @click="menu=4"> <a href="#">Categorias</a> </li>
            <li @click="menu=5"> <a href="#">Unidades</a> </li>
            <li @click="menu=6"> <a href="#">Cert. y Credenciales</a> </li>
          </ul>
        </li>

         <li>
          <a class="animate__animated animate__bounce" href="#" data-toggle="collapse" data-target="#biblioteca">
            <div class="pull-left"><i class="ti-book"></i><span class="right-nav-text">Biblioteca INSO</span></div>
            <div class="pull-right"><i class="ti-plus"></i></div><div class="clearfix"></div>
          </a>
          <ul id="biblioteca" class="collapse" data-parent="#sidebarnav">
            <li @click="menu=7"> <a href="#">Estado</a> </li>
            <li @click="menu=8"> <a href="#">Libros</a> </li>
            <li @click="menu=9"> <a href="#">Prestamo</a> </li>
          </ul>
        </li> --}}

        {{-- <li>
          <a href="#" data-toggle="collapse" data-target="#chart">
            <div class="pull-left"><i class="ti-pencil-alt"></i><span class="right-nav-text">Accidentes de Trabajo</span></div>
            <div class="pull-right"><i class="ti-plus"></i></div><div class="clearfix"></div>
          </a>
          <ul id="chart" class="collapse" data-parent="#sidebarnav">
            <li @click="menu=7"> <a href="#">Razón Social</a> </li>
            <li @click="menu=8"> <a href="#">Afps</a> </li>
            <li @click="menu=9"> <a href="#">Grupo</a> </li>
            <li @click="menu=10"> <a href="#">Ocupaciones</a> </li>
            <li @click="menu=11"> <a href="#">Accidentes</a> </li>
            <li @click="menu=12"> <a href="#">Tipo Accidente</a> </li>
            <li @click="menu=13"> <a href="#">Formulario</a> </li>
          </ul>
        </li> --}}

        {{-- <li>
          <a href="#" data-toggle="collapse" data-target="#users">
            <div class="pull-left"><i class="ti-user"></i><span class="right-nav-text">Usuarios</span></div>
            <div class="pull-right"><i class="ti-plus"></i></div><div class="clearfix"></div>
          </a>
          <ul id="users" class="collapse" data-parent="#sidebarnav">
            <li @click="menu=14"> <a href="#">Roles</a> </li>
            <li @click="menu=15"> <a href="#">Usuarios</a> </li>
          </ul>
        </li> --}}

   </ul>
 </div>
</div>
