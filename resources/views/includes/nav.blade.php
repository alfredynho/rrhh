<nav class="admin-header navbar navbar-default col-lg-12 col-12 p-0 fixed-top d-flex flex-row"> {{-- header-dark --}}
    <div class="text-left navbar-brand-wrapper">
        <a class="navbar-brand brand-logo" href="#"><img src="images/inso.png" alt="INSTITUTO NACIONAL DE SALUD OCUPACIONAL" ></a>
        <a class="navbar-brand brand-logo-mini" href="#"><img src="images/inso.png" alt="INSTITUTO NACIONAL DE SALUD OCUPACIONAL"></a>
    </div>

    <ul class="nav navbar-nav mr-auto">
        <li class="nav-item">
        <a id="button-toggle" class="button-toggle-nav inline-block ml-20 pull-left" href="javascript:void(0);"><img src="{{ asset('images/menu.png') }}"></a>
        </li>
    </ul>

    <ul class="nav navbar-nav ml-auto">
        <li class="nav-item fullscreen">
        <a id="btnFullscreen" href="#" class="nav-link"><img src="{{ asset('images/fullscreen.png') }}"></a>
        </li>
        <li class="nav-item dropdown ">
        <a class="nav-link top-nav" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{ asset('images/notificacion.png') }}" alt="Notificaciones">
            <span class="badge badge-danger notification-status"> </span>
        </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-big dropdown-notifications">
            <div class="dropdown-header notifications">
            <strong>Notificaciones</strong>
            <span class="badge badge-pill badge-warning">05</span>
            </div>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">New registered user <small class="float-right text-muted time">Just now</small> </a>
            <a href="#" class="dropdown-item">Order confirmation<small class="float-right text-muted time">2 days</small> </a>
        </div>
        </li>
        <li class="nav-item dropdown ">
        <a class="nav-link top-nav" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true"> <i class=" ti-view-grid"></i> </a>
        <div class="dropdown-menu dropdown-menu-right dropdown-big">
            <div class="dropdown-header">
            <strong>Operaciones</strong>
            </div>
            <div class="dropdown-divider"></div>
            <div class="nav-grid">
                <a href="#" class="nav-grid-item"><i class="ti-truck text-danger "></i><h5>Ordenes</h5></a>
                <a href="#" class="nav-grid-item"><i class="ti-check-box text-success"></i><h5>Tareas Asiganadas</h5></a>
            </div>
        </div>
        </li>
        <li class="nav-item dropdown mr-30">
        <a class="nav-link nav-pill user-avatar" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
            <img src="{{ asset('images/user_admin.png') }}" alt="Usuario Admin">
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header">
            <div class="media">
                <div class="media-body">
                <h5 class="mt-0 mb-0"> {{ getUser()->nombres }} {{ getUser()->paterno }} {{ getUser()->materno }}</h5>
                <span>
                    @if(Auth::user()->idrol == 1)
                        Administrador
                    @elseif (Auth::user()->idrol == 2)
                        Editor
                    @elseif ( Auth::user()->idrol == 3)
                        Invitado
                    @endif
                </span>
                </div>
            </div>
            </div>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#"><img src="{{ asset('images/mensajes.png') }}"> &nbsp;Mensajes</a>
            <a @click="menu=12" class="dropdown-item" href="#"><img src="{{ asset('images/user_admin.png') }}"> &nbsp;Perfil</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#"><img src="{{ asset('images/configuracion.png') }}"> &nbsp;Configuraciones</a>

            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><img src="{{ asset('images/salir.png') }}"> &nbsp;Salir</a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>

        </div>
        </li>
    </ul>
    </nav>
