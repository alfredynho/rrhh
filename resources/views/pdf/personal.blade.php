<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>REPORTE DE PERSONAL - {{ $personal->nombres }} {{ $personal->paterno }} {{ $personal->materno }} </title>
    <style>
          .clearfix:after {
            content: "";
            display: table;
            clear: both;
          }

          a {
            color: #0087C3;
            text-decoration: none;
          }

          body {
            position: relative;
            width: 21cm;
            height: 29.7cm;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-family: Arial, sans-serif;
            font-size: 14px;
          }

          #logo {
            float: left;
            margin-top: 10px;
          }

          #logo img {
            height: 150px;
          }

          #company {
            float: right;
            text-align: right;
          }


          #details {
            margin-bottom: 50px;
          }

          #client {
            padding-left: 6px;
            border-left: 6px solid #57B223;
            float: left;
          }

          #client .to {
            color: #777777;
          }

          h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
          }

          #invoice {
            float: right;
            text-align: right;
          }

          #invoice h1 {
            color: #0087C3;
            font-size: 2.4em;
            line-height: 1em;
            font-weight: normal;
            margin: 0  0 10px 0;
          }

          #invoice .date {
            font-size: 1.1em;
            color: #777777;
          }

          table {
            width: 90%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
          }

          table th,
          table td {
            padding: 20px;
            background: #EEEEEE;
            text-align: center;
            border-bottom: 1px solid #FFFFFF;
          }

          table th {
            white-space: nowrap;
            font-weight: normal;
          }

          table td {
            text-align: right;
          }

          table td h3{
            color: #57B223;
            font-size: 1.2em;
            font-weight: normal;
            margin: 0 0 0.2em 0;
          }

          table .no {
            color: #FFFFFF;
            font-size: 1.6em;
            background: #57B223;
          }

          table .desc {
            text-align: left;
          }

          table .unit {
            background: #DDDDDD;
          }

          table .qty {
          }

          table .total {
            background: #57B223;
            color: #FFFFFF;
          }

          table td.unit,
          table td.qty,
          table td.total {
            font-size: 1.2em;
          }

          table tbody tr:last-child td {
            border: none;
          }

          table tfoot td {
            padding: 10px 20px;
            background: #FFFFFF;
            border-bottom: none;
            font-size: 1.2em;
            white-space: nowrap;
            border-top: 1px solid #AAAAAA;
          }

          table tfoot tr:first-child td {
            border-top: none;
          }

          table tfoot tr:last-child td {
            color: #57B223;
            font-size: 1.4em;
            border-top: 1px solid #57B223;

          }

          table tfoot tr td:first-child {
            border: none;
          }

          #thanks{
            font-size: 1em;
            margin-bottom: 50px;
          }

          #notices{
            padding-left: 6px;
            border-left: 6px solid #57B223;
          }

          #notices .notice {
            font-size: 1.2em;
          }

          footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
          }
    </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="../public/images/cabecera.png">
      </div>
    </header>
    <hr color="green" size=1 width="250">

    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <div class="to">REPORTE DE PERSONAL:</div>
          <h2 class="name">{{ $personal->nombres }} {{ $personal->paterno }} {{ $personal->materno }}</h2>
          <div class="address">{{ $personal->cedula }}</div>
          <div class="email">{{ $personal->correo }}</div>
        </div>
      </div>
      <table width="100%" class="tabla2">
        <thead>
          <tr>
            <th class="no">#</th>
            <th class="desc" colspan="4"> <strong>NOMBRE: {{ $personal->nombres }} {{ $personal->paterno }} {{ $personal->materno }} </strong></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="width: 0.1px" class="no"></td>
            <td class="desc"><h3>DIRECCIÓN</h3>{{ strtoupper($personal->direccion) }}</td>
            <td class="desc"><h3>ESTADO CIVIL</h3>{{ estadoCivil($personal->estado_civil) }}</td>
            <td class="desc"><h3>CORREO:</h3>{{ $personal->correo }}</td>
            <td class="desc"><h3>SEXO</h3>{{ $personal->genero }}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">SUBTOTAL</td>
            <td>$5,200.00</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">TAX 25%</td>
            <td>$1,300.00</td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td colspan="2">GRAND TOTAL</td>
            <td>$6,500.00</td>
          </tr>
        </tfoot>
      </table>
      <div id="thanks">______________________</div>
      <div id="notices">
        <div>INSTITUTO NACIONAL DE SALUD OCUPACIONAL</div>
        <div class="notice">Al Servivicio del País desde 1962</div>
      </div>
    </main>
  </body>
</html>
