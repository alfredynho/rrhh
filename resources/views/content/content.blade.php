@extends('base')
    @section('content')

    @if(Auth::check())
            @if (Auth::user()->idrol == 1)
        <transition enter-active-class="animated bounceInLeft">
        <template v-if="menu==0">
            <dashboard></dashboard>
        </template>

        <template v-if="menu==1">
            <unidad></unidad>
        </template>

        <template v-if="menu==2">
            <profesion></profesion>
        </template>

        <template v-if="menu==3">
            <cargo></cargo>
        </template>

        <template v-if="menu==4">
            <personal></personal>
        </template>

        <template v-if="menu==5">
            <vacaciones></vacaciones>
        </template>

        <template v-if="menu==6">
            <vacaciones></vacaciones>
        </template>

        <template v-if="menu==11">
            <calendar></calendar>
        </template>

        @elseif (Auth::user()->idrol == 2)

        <template v-if="menu==12">
            <perfil></perfil>
        </template>

        <template v-if="menu==13">
            <asignacion></asignacion>
        </template>

        @elseif (Auth::user()->idrol == 3)
            <template v-if="menu==14">
                <menupersona></menupersona>
            </template>
        @else

        @endif

        @endif

        {{--
        <template v-if="menu==2">
            <client></client>
        </template>

        <template v-if="menu==3">
            <provider></provider>
        </template>

        <template v-if="menu==4">
            <status></status>
        </template>

        <template v-if="menu==5">
            <units></units>
        </template>

        <template v-if="menu==6">
            <tasks></tasks>
        </template>

        <template v-if="menu==7">
            <razonsocial></razonsocial>
        </template>

        <template v-if="menu==8">
            <afps></afps>
        </template>

        <template v-if="menu==9">
            <grupo></grupo>
        </template>

        <template v-if="menu==10">
            <ocupaciones></ocupaciones>
        </template>

        <template v-if="menu==11">
            <accidentes></accidentes>
        </template>

        <template v-if="menu==12">
            <accidentes></accidentes>
        </template>

        <template v-if="menu==13">
            <accidentes></accidentes>
        </template>

        <template v-if="menu==14">
            <rol></rol>
        </template>

        <template v-if="menu==15">
            <profile></profile>
        </template>

        <template v-if="menu==16">
            <users></users>
        </template>

        <template v-if="menu==22">
            <profile></profile>
        </template> --}}

        </transition>

@endsection('content')
