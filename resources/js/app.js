require("./bootstrap");

import Vue from "../../public/js/vue.js";

import dashboard from "./components/Dashboard.vue";
import unidad from "./components/Unidad";
import profesion from "./components/Profesion";
import cargo from "./components/Cargo";
import personal from "./components/Personal";
import afp from "./components/Afps";
import vacaciones from "./components/Vacaciones";
import calendar from "./components/Calendar";
import perfil from "./components/Profile";
import capacitaciones from "./components/Capacitacion";
import salidas from "./components/Salidas";
import usuario from "./components/Usuario";
import asignacion from "./components/Asignacion";
import menupersona from "./components/Menupersona";

const app = new Vue({
    el: "#app",
    data: {
        menu: 0
    },
    components:{
        dashboard,
        unidad,
        profesion,
        cargo,
        personal,
        afp,
        vacaciones,
        calendar,
        perfil,
        capacitaciones,
        salidas,
        usuario,
        asignacion,
        menupersona,
    }
});


