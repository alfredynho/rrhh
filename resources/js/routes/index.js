import Vue from 'vue'
import Router from 'vue-router'

import dashboard from "./components/Dashboard.vue";
import category from "./components/Category.vue";
import profile from "./components/Profile.vue";
import form from "./components/Form";
import unidad from "./components/Unidad";
import profesion from "./components/Profesion";
import cargo from "./components/Cargo";
import personal from "./components/Personal";
import calendar from "./components/Calendar";
import perfil from "./components/Profile";
import menupersona from './components/Menupersona';

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/home',
      name: 'home',
      component: home
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: dashboard
    },
    {
      path: '/categoria',
      name: 'categoria',
      component: category
    },
    {
      path: '/formulario',
      name: 'form',
      component: form
    },
    {
      path: '/roles',
      name: 'roles',
      component: rol
    },
    {
      path: '/unidad',
      name: 'unidad',
      component: unidad
    },

    {
      path: '/profesion',
      name: 'profesion',
      component: profesion
    },

    {
       path: '/cargo',
       name: 'cargo',
       component: cargo
    },
    {
        path: '/personal',
        name: 'personal',
        component: personal
     },
     {
        path: '/calendario',
        name: 'calendar',
        component: calendar
     },
     {
        path: '/vacaciones',
        name: 'vacaciones',
        component: vacaciones
     },
     {
        path: '/capacitaciones',
        name: 'capacitaciones',
        component: capacitaciones
     },
     {
        path: '/salidas',
        name: 'salidas',
        component: salidas
     },
     {
        path: '/usuario',
        name: 'usuario',
        component: usuario
     },
     {
        path: '/asignacion',
        name: 'asignacion',
        component: asignacion
     },
     {
        path: '/perfil',
        name: 'perfil',
        component: perfil
     },
     {
        path: '/menupersona',
        name: 'menupersona',
        component: menupersona
     },
  ]
})
